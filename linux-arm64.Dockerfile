FROM ghcr.io/hotio/base:focal-f190a37-219-linux-amd64 as builder

RUN curl -fsSL "https://github.com/balena-io/qemu/releases/download/v4.0.0-balena/qemu-4.0.0-balena-aarch64.tar.gz" | tar xzf - -C . --strip-components=1

FROM ghcr.io/hotio/base:focal-f190a37-219-linux-arm64

ARG DEBIAN_FRONTEND="noninteractive"

ENV DEEMIX_DATA_DIR="/config/app"
ENV DEEMIX_MUSIC_DIR="/downloads"

EXPOSE 6595

COPY --from=builder qemu-aarch64-static /usr/bin

# install packages
RUN apt update && \
    apt install -y --no-install-recommends --no-install-suggests \
        netbase build-essential gcc-aarch64-linux-gnu python3.8-minimal python3.8-dev python3-pip opus-tools && \
# clean up
    apt autoremove -y && \
    apt clean && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

ARG VERSION
ARG PACKAGE_VERSION=${VERSION}
RUN mkdir "${APP_DIR}/bin" && \
    curl -fsSL "https://gitlab.com/RemixDev/deemix-gui-pyweb/-/archive/main/deemix-gui-pyweb-main.tar.gz" | tar xzf - -C "${APP_DIR}/bin" --strip-components=1 && \
    curl -fsSL "https://gitlab.com/RemixDev/deemix-webui/-/archive/main/deemix-webui-main.tar.gz" | tar xzf - -C "${APP_DIR}/bin/webui" --strip-components=1 && \
    sed -i 's/x\['\''title'\''\] == '\''Music'\''/'\''module_id=83718b7b-5503-4062-b8b9-3530e2e2cefa'\'' in x['\''section_id'\'']/g' "${APP_DIR}/bin/app.py" && \
    pip3 --disable-pip-version-check --no-cache-dir install -r "${APP_DIR}/bin/server-requirements.txt" && \
    apt autoremove --purge -y build-essential gcc-aarch64-linux-gnu python3.8-dev python3-pip && \
    chmod -R u=rwX,go=rX "${APP_DIR}" && \
    mkdir -p "${DEEMIX_MUSIC_DIR}" && \
    chmod -R u=rwX,go=rX "${DEEMIX_MUSIC_DIR}" && \
    rm /usr/bin/qemu-aarch64-static

COPY root/ /
